package com.manabun.engine;

import java.awt.event.KeyEvent;

import com.manabun.engine.gfx.Image;
import com.manabun.engine.listeners.Observable;

public class StaticObject extends Object{
	private boolean solid = false;
	private boolean currentlySolid = false;
	public StaticObject(Image Texture, String Name, int x, int y) {
		this.setDead(false);
		this.setTexture(Texture);
		this.setName(Name);
		this.setPositionX(x);
		this.setPositionY(y);
		this.setHeight(this.getTexture().getHeight());
		this.setWidth(this.getTexture().getWidth());
	}
	
	public StaticObject(Image Texture, String Name, int x, int y, boolean solid) {
		this.setDead(false);
		this.setTexture(Texture);
		this.setName(Name);
		this.setPositionX(x);
		this.setPositionY(y);
		this.setHeight(this.getTexture().getHeight());
		this.setWidth(this.getTexture().getWidth());
		this.setSolid(solid);
	}
	

	@Override
	public void update(GameContainer gc, float dt) {
		if(!this.currentlySolid && this.solid){
			this.addCollisionPostion(gc);
		}
    	if (gc.getInput().isKey(KeyEvent.VK_K))
        {
        	this.remove(gc);

        }
		this.render(gc, gc.getRender());
	}

	//all of these will need some form of bounds check
	private void removeCollision(GameContainer gc) {
		for(int i = this.positionX; i < this.positionX + this.width; ++i){
			for(int j = this.positionY; j <  this.positionY + this.height; ++j){
				gc.collisionMap[i][j] = false;
			}
		}
	}
	
	private void addCollisionPostion(GameContainer gc) {
		for(int i = this.positionX; i < this.positionX + this.width; ++i){
			for(int j = this.positionY; j <  this.positionY + this.height; ++j){
				gc.collisionMap[i][j] = true;
			}
		}
		this.currentlySolid = true;
		
	}


	@Override
	public void render(GameContainer gc, Renderer r) {
		// TODO Auto-generated method stub
		r.drawImage(this.getTexture(), this.getPositionX(), this.getPositionY(), 0, 0);
		
	}
	
	@Override
	public void register(Observable o){
		o.addObserver(this);
	}
	
	@Override
	public void unRegister(Observable o){
		o.removeObserver(this);
	}
	
	public void remove(GameContainer gc){
		this.removeCollision(gc);
		this.setDead(true);
	}

	@Override
	public boolean collisionCheck(GameContainer gc) {
		// TODO Auto-generated method stub

//		for(int i = this.positionX; i < this.positionX + this.width; ++i){
//			for(int j = this.positionY; j <  this.positionY + this.height; ++j){
//				if(gc.collisionMap[i][j] == true){
//					System.out.println("boom bitch");
//					return true;
//				}
//			}
//		}
		return true;
	}
	
	public void setSolid(boolean s){
		this.solid = true;
	}
	
	public boolean getSolid(){
		return this.solid;
	}



}

