package com.manabun.engine;


import java.awt.event.KeyEvent;
import com.manabun.engine.gfx.Image;
import com.manabun.engine.listeners.Observable;

import game.GameManager;

public class PlayerObject extends Object{
	private float dtUpdate = 0;
	public PlayerObject(Image Texture, String Name) {
		this.setDead(false);
		this.setTexture(Texture);
		this.setName(Name);
		this.setPositionX(60);
		this.setPositionY(60);
		this.setHeight(this.getTexture().getHeight());
		this.setWidth(this.getTexture().getWidth());
	}

	@Override
	public void update(GameContainer gc, float dt) {
		// TODO Auto-generated method stub
		this.removeCollision(gc);
        if (dtUpdate > 1)
		{
			dtUpdate = 0;
			System.out.println("updated");
			if (gc.getInput().isKey(KeyEvent.VK_W)) {
				this.setPositionY(this.getPositionY() - 1);
				if(this.collisionCheck(gc)){
					this.setPositionY(this.getPositionY() + 1);
				}

			}
			if (gc.getInput().isKey(KeyEvent.VK_A)) {
				this.setPositionX(this.getPositionX() - 1);
				if(this.collisionCheck(gc)){
					this.setPositionX(this.getPositionX() + 1);
				}

			}
			if (gc.getInput().isKey(KeyEvent.VK_S)) {
				this.setPositionY(this.getPositionY() + 1);
				if(this.collisionCheck(gc)){
					this.setPositionY(this.getPositionY() - 1);
				}

			}

			if (gc.getInput().isKey(KeyEvent.VK_D)) {
				this.setPositionX(this.getPositionX() + 1);
				if(this.collisionCheck(gc)){
					this.setPositionX(this.getPositionX() - 1);
				}

			}
		}else{
        	dtUpdate = dtUpdate + dt;
        	System.out.println(dtUpdate);
		}
        this.updateCollisionPostion(gc);
        this.render(gc, gc.getRender());
	}

	//all of these will need some form of bounds check
	private void removeCollision(GameContainer gc) {
		for(int i = this.positionX; i < this.positionX + this.width; ++i){
			for(int j = this.positionY; j <  this.positionY + this.height; ++j){
				gc.collisionMap[i][j] = false;
			}
		}
	}

	private void updateCollisionPostion(GameContainer gc) {
		for(int i = this.positionX; i < this.positionX + this.width; ++i){
			for(int j = this.positionY; j <  this.positionY + this.height; ++j){
				gc.collisionMap[i][j] = true;
			}
		}
		
	}

	@Override
	public void render(GameContainer gc, Renderer r) {
		// TODO Auto-generated method stub
		r.drawImage(this.getTexture(), this.getPositionX(), this.getPositionY(), 0, 0);
		
	}
	
	@Override
	public void register(Observable o){
		o.addObserver(this);
	}
	
	@Override
	public void unRegister(Observable o){
		o.removeObserver(this);
	}
	

	@Override
	public boolean collisionCheck(GameContainer gc) {
		// TODO Auto-generated method stub

		for(int i = this.positionX; i < this.positionX + this.width; ++i){
			for(int j = this.positionY; j <  this.positionY + this.height; ++j){
				if(gc.collisionMap[i][j] == true){
					System.out.println("boom bitch");
					return true;
				}
			}
		}
		return false;
	}



}
