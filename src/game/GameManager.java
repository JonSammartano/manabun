package game;

import com.manabun.engine.AbstractGame;
import com.manabun.engine.GameContainer;
import com.manabun.engine.Renderer;
import com.manabun.engine.StaticObject;
import com.manabun.engine.Object;
import com.manabun.engine.PlayerObject;
import com.manabun.engine.audio.SoundClip;
import com.manabun.engine.gfx.Image;
import com.manabun.engine.gfx.ImageTile;
import com.manabun.engine.gfx.Light;
import com.manabun.engine.listeners.Observable;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Iterator;

public class GameManager extends AbstractGame implements Observable
{

    private Image image;
    private Image backgroundImage;
    private Image imageAlpha;
    private Image imageShadow;
    private Image imageWall;
    private ImageTile imageTile;
    private Light light;
    private SoundClip clip;
    private ArrayList<Object> observers = new ArrayList<Object>();

    public GameManager()
    {
        backgroundImage = new Image("/sprites/paper.png");
        imageShadow = new Image("/sprites/pine.png");
        imageWall = new Image("/sprites/wall.png");
        imageShadow.setLightBlock(Light.FULL);
        image = new Image("/sprites/engineTest.png");
        imageTile = new ImageTile("/tilesets/tilesetTest.png", 16, 16);
        clip = new SoundClip("/audio/audioTest.wav");
        imageAlpha = new Image("/sprites/alphaTest.png");
        imageAlpha.setAlpha(true);
        light = new Light(50, 0xffffffff);
        light.setDiameter(100);
        new PlayerObject(imageShadow, "player").register(this);
        new StaticObject(imageWall, "wall", 0, 0,true).register(this);
       
        
    }

    @Override
    public void update(GameContainer gc, float dt)
    {

    }

    @Override
    public void render(GameContainer gc, Renderer r, float dt)
    {
    	
    	
        r.drawImage(backgroundImage, 0, 0,0,0);

//        r.drawText("test", 50, 50, 0xFFFFFsdFFF);
//        r.drawImage(image, 0, 128,0,0);
        
//        r.setzDepth(Integer.MAX_VALUE);
        notifyObservers(gc, dt);
        //r.drawImage(imageAlpha, gc.getInput().getMouseX() - 8, gc.getInput().getMouseY() - 8);
//        r.drawImage(imageTile, 0, 8, (int)tempX, (int)tempY);
        //r.drawFilledRect(gc.getInput().getMouseX() - 16, gc.getInput().getMouseY() - 16, 32, 32, 0xffffffff);
//        r.drawImage(imageShadow, 64, 64,0,0);
//        r.drawLight(light, gc.getInput().getMouseX(), gc.getInput().getMouseY());
    }
    


	@Override
	public void notifyObservers(GameContainer gc, float dt) {
		Object o = null;
		for (Iterator<Object> iterator = observers.iterator(); iterator.hasNext();) {
			o = iterator.next();
			if(o.isDead()){
				o.unRegister(this);
				iterator.remove();
			}
			o.update(gc, dt);
		}
		
	}

    public static void main(String args[])
    {
        GameContainer gc = new GameContainer(new GameManager());
        gc.start();
    }

	@Override
	public void addObserver(Object o) {
		observers.add(o);
		
		
	}

	@Override
	public void removeObserver(Object o) {
		
	}
    
	
}
