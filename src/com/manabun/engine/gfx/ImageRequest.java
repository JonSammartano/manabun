package com.manabun.engine.gfx;

public class ImageRequest
{
    public Image image;
    public int zDepth;
    public int offsetX, offsetY;

    public ImageRequest(Image image, int zDepth, int offsetX, int offsetY)
    {
        this.image = image;
        this.zDepth = zDepth;
        this.offsetY = offsetY;
        this.offsetX = offsetX;
    }
}
