package com.manabun.engine;

import java.util.BitSet;

public class GameContainer implements Runnable
{
	private Thread thread;
	private Window window;
	private Renderer renderer;
	private Input input;
	private AbstractGame game;

	private boolean running = false;
	private final double UPDATE_CAP = 1.0 / 60.0;
	private int width = 320, height = 180;
	private float scale = 4f;
	private String title = "Manabun Engine ver. 1.0f";
	public boolean[][] collisionMap;

	public Window getWindow() {
		return window;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public float getScale() {
		return scale;
	}
	public void setScale(float scale) {
		this.scale = scale;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	public GameContainer(AbstractGame game)
	{
		this.game = game;
	}
	
	public void start()
	{
		collisionMap = new boolean[this.width][this.height];
		window = new Window(this);
		thread = new Thread(this);
		renderer = new Renderer(this);
		input = new Input(this);
		thread.run();
	}
	
	public void stop()
	{
		
	}

	public Input getInput() {
		return input;
	}

	public void run()
	{
		running = true;
		boolean render = false;
		double firstTime = 0;
		double lastTime = System.nanoTime() / 1000000000.0;
		double passedTime = 0;
		double unprocessedTime = 0;
		double frameTime = 0;
		int frames = 0;
		int fps = 0;
		
		while(running)
		{
			render = true; //This is the FPS capper. Make true to uncap framerate
			
			firstTime = System.nanoTime() / 1000000000.0;
			passedTime = firstTime - lastTime;
			lastTime = firstTime;
			unprocessedTime += passedTime;
			frameTime += passedTime;
			
			while (unprocessedTime >= UPDATE_CAP)
			{
				unprocessedTime -= UPDATE_CAP;
				render = true;

				//ASD
				game.update(this, (float)UPDATE_CAP);

				input.update();

				if (frameTime >= 1.0)

				{
					frameTime = 0;
					fps = frames;
					frames = 0;
					//System.out.println(fps);
				}
			}
			
			if (render)
			{
				renderer.clear();
				game.render(this, renderer, (float)UPDATE_CAP);
				renderer.process();
				renderer.drawText("FPS: " + fps, 0, 0, 0xffffffff);
				window.update();
				frames++;
			}
			else
			{
				try {
					Thread.sleep(1);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		dispose();
	}
	
	public void dispose()
	{
		
	}
	public Renderer getRender(){
		return this.renderer;
	}
}
