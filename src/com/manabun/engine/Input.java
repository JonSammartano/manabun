package com.manabun.engine;

import java.awt.event.*;

public class Input implements KeyListener, MouseListener, MouseMotionListener, MouseWheelListener
{
    private GameContainer gc;

    private final int NUM_KEYS = 256;
    private final int NUM_BUTTONS = 5;

    //Keyboard
    private boolean[] keys = new boolean[NUM_KEYS];
    private boolean[] keysLast = new boolean[NUM_KEYS];

    //Mouse
    private boolean[] buttons = new boolean[NUM_BUTTONS];
    private boolean[] buttonsLast = new boolean[NUM_BUTTONS];
    private int mouseX, mouseY;
    private int scroll;

    public Input(GameContainer gc)
    {
        this.gc = gc;
        mouseX = 0;
        mouseY = 0;
        scroll = 0;

        gc.getWindow().getCanvas().addKeyListener(this);
        gc.getWindow().getCanvas().addMouseListener(this);
        gc.getWindow().getCanvas().addMouseMotionListener(this);
        gc.getWindow().getCanvas().addMouseWheelListener(this);

    }

    public void update()
    {
        scroll = 0;

        for(int i = 0; i < NUM_KEYS; i++)
        {
            keysLast[i] = keys[i];
        }

        for(int i = 0; i < NUM_BUTTONS; i++)
        {
            buttonsLast[i] = buttons[i];
        }
    }

    //KEYBOARD
    public boolean isKey(int keyCode)
    {
        return keys[keyCode];
    }

    public boolean isKeyUp(int keyCode)
    {
        return !keys[keyCode] && keysLast[keyCode];
    }

    public boolean isKeyDown(int keyCode)
    {
        return keys[keyCode] && !keysLast[keyCode];
    }

    //MOUSE
    public boolean isButton(int button)
    {
        return buttons[button];
    }

    public boolean isButtonUp(int button)
    {
        return !buttons[button] && buttonsLast[button];
    }

    public boolean isButtonDown(int button)
    {
        return buttons[button] && !buttonsLast[button];
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent arg0)
    {
        scroll = arg0.getWheelRotation();
    }

    @Override
    public void mouseDragged(MouseEvent arg0)
    {
        mouseX = (int)(arg0.getX() / gc.getScale());
        mouseY = (int)(arg0.getY() / gc.getScale());
    }

    public int getMouseX() {
        return mouseX;
    }

    public int getMouseY() {
        return mouseY;
    }

    public int getScroll() {
        return scroll;
    }

    @Override
    public void mouseMoved(MouseEvent arg0)
    {
        mouseX = (int)(arg0.getX() / gc.getScale());
        mouseY = (int)(arg0.getY() / gc.getScale());
    }

    @Override
    public void mouseClicked(MouseEvent arg0)
    {

    }

    @Override
    public void mouseEntered(MouseEvent arg0)
    {

    }

    @Override
    public void mouseExited(MouseEvent arg0)
    {

    }

    @Override
    public void mousePressed(MouseEvent arg0)
    {
        buttons[arg0.getButton()] = true;
    }

    @Override
    public void mouseReleased(MouseEvent arg0)
    {
        buttons[arg0.getButton()] = false;
    }

    @Override
    public void keyPressed(KeyEvent arg0)
    {
        keys[arg0.getKeyCode()] = true;
    }

    @Override
    public void keyReleased(KeyEvent arg0)
    {
        keys[arg0.getKeyCode()] = false;
    }

    @Override
    public void keyTyped(KeyEvent arg0)
    {

    }
}
