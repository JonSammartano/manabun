package com.manabun.engine;

import com.manabun.engine.gfx.Image;
import com.manabun.engine.listeners.Observable;

import game.GameManager;

/**
 *
 * @author Ballsack Johnson
 */
public abstract class Object 
{
    protected String name;
    protected int positionX, positionY;
    protected int width, height;
    protected boolean dead = true;
    protected Image texture = null;
    public abstract void update(GameContainer gc, float dt);
    public abstract void render(GameContainer gc, Renderer r);

    
    public abstract boolean collisionCheck(GameContainer gc);
    public abstract void register(Observable o);
    public abstract void unRegister(Observable o);
    
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPositionX() {
        return positionX;
    }

    public void setPositionX(int positionX) {
        this.positionX = positionX;
    }

    public int getPositionY() {
        return positionY;
    }

    public void setPositionY(int positionY) {
        this.positionY = positionY;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
	public boolean isDead() {
		return dead;
	}
	public void setDead(boolean dead) {
		this.dead = dead;
	}
	public Image getTexture() {
		return texture;
	}
	public void setTexture(Image texture) {
		this.texture = texture;
	}
    
}
