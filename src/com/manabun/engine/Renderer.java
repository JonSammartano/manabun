package com.manabun.engine;

import com.manabun.engine.gfx.Font;
import com.manabun.engine.gfx.Image;
import com.manabun.engine.gfx.ImageRequest;
import com.manabun.engine.gfx.ImageTile;
import com.manabun.engine.gfx.Light;
import com.manabun.engine.gfx.LightRequest;

import java.awt.image.DataBufferInt;
import java.util.ArrayList;

public class Renderer
{
    //p stands for pixel sorry for lazy
    private int pixelWidth, pixelHeight;
    private int[] pixel;
    private int[] zBuffer;
    private int zDepth = 0;
    private boolean processing = false;
    private Font font = Font.DEFAULT;
    private ArrayList<ImageRequest> imageRequest = new ArrayList<ImageRequest>();
    private ArrayList<LightRequest> lightRequest = new ArrayList<LightRequest>();
    private int[] lightMap;
    private int[] lightBlock;
    private int ambientColor = 0xff6b6b6b;
    
    
    public int getzDepth() {
        return zDepth;
    }

    public void setzDepth(int zDepth) {
        this.zDepth = zDepth;
    }

    public Renderer(GameContainer gc)
    {
        pixelWidth = gc.getWidth();
        pixelHeight = gc.getHeight();
        pixel = ((DataBufferInt)gc.getWindow().getImage().getRaster().getDataBuffer()).getData();
        zBuffer = new int[pixel.length];
        lightMap = new int[pixel.length];
        lightBlock = new int[pixel.length];
    }

    public void clear()
    {
        for (int i = 0; i < pixel.length; i++)
        {
            pixel[i] = 0;
            zBuffer[i] = 0;
            lightMap[i] = ambientColor;
            lightBlock[i] = 0;
        }
    }

    public void process()
    {
        processing = true;
        for(int i = 0; i < imageRequest.size(); i++)
        {
            ImageRequest ir = imageRequest.get(i);
            setzDepth(ir.zDepth);
            drawImage(ir.image, ir.offsetX, ir.offsetY,0,0);
        }
        
        //have to draw lights AFTER alpha images in order for it to work on them
        for (int i = 0; i < lightRequest.size(); i++)
        {
            LightRequest lReq = lightRequest.get(i);
            this.drawLightRequest(lReq.light, lReq.locationX, lReq.locationY);
        }
        
        for (int i = 0; i < pixel.length; i++)
        {
            float red = ((lightMap[i] >> 16) & 0xff) / 255f;
            float green = ((lightMap[i] >> 8) & 0xff) / 255f;
            float blue = (lightMap[i] & 0xff) / 255f;
            pixel[i] = ((int)(((pixel[i] >> 16) & 0xff) * red) << 16 | (int)(((pixel[i] >> 8) & 0xff) * green) << 8 | (int)((pixel[i] & 0xff) * blue));
        }
        
        imageRequest.clear();
        lightRequest.clear();
        processing = false;
    }

    public void setPixel(int x, int y, int value)
    {
        int alpha = ((value >> 24) & 0xff);

        if ((x < 0 || x >= pixelWidth || y < 0 || y >= pixelHeight) || alpha == 0)
            return; //out of bounds
        if (zBuffer[x + y * pixelWidth] > zDepth)
            return;

        if (alpha == 255)
        {
            pixel[x + y * pixelWidth] = value;
        }
        else
        {
            int pixelColor = pixel[x + y * pixelWidth];

            int newRed = ((pixelColor >> 16) & 0xff) - (int)((((pixelColor >> 16) & 0xff) - (value >> 16) & 0xff) * (alpha / 255f));
            int newGreen = ((pixelColor >> 8) & 0xff) - (int)((((pixelColor >> 8) & 0xff) - (value >> 8) & 0xff) * (alpha / 255f));
            int newBlue = (pixelColor & 0xff) - (int)((((pixelColor & 0xff)) - (value & 0xff)) * (alpha / 255f));
            pixel[x + y * pixelWidth] = (newRed << 16 | newGreen << 8 | newBlue);
            //p[x + y * pW] = (255 << 24 | newRed << 16 | newGreen << 8 | newBlue);
        }
    }

    public void setLightMap(int x, int y, int value)
    {
        if (x < 0 || x >= pixelWidth || y < 0 || y >= pixelHeight)
            return; //out of bounds
        
        int baseColor = lightMap[x + y * pixelWidth];
        int maxRed, maxGreen, maxBlue;
        maxRed = Math.max((baseColor >> 16) & 0xff, (value >> 16) & 0xff);
        maxGreen = Math.max((baseColor >> 8) & 0xff, (value >> 8) & 0xff);
        maxBlue = Math.max(baseColor& 0xff, value & 0xff);
        lightMap[x + y * pixelWidth] = (maxRed << 16 | maxGreen << 8 | maxBlue);
    }
    
        public void setLightBlock(int x, int y, int value)
    {
        if (x < 0 || x >= pixelWidth || y < 0 || y >= pixelHeight)
            return; //out of bounds
        if (zBuffer[x + y * pixelWidth] > zDepth)
            return;
        lightBlock[x + y * pixelWidth] = value;
    }
    
    public void drawText(String text, int offsetX, int offsetY, int color)
    {
    	
        int offset = 0;

        for (int i = 0; i < text.length(); i++)
        {
            int unicode = text.codePointAt(i);
            for(int y = 0; y < font.getFontImage().getHeight(); y++)
            {
                for (int x = 0; x < font.getWidths()[unicode]; x++)
                {
                    if (font.getFontImage().getPixel()[(x + font.getOffsets()[unicode]) + y * font.getFontImage().getWidth()] == 0xffffffff)
                    {
                        setPixel(x + offset + offsetX, y + offsetY, color);
                    }
                }
            }
            offset += font.getWidths()[unicode];
        }
    }
    

    /**
     * @description draw and an image or an image tile to the screen
   	 *
     * @param image
     * @param offsetX
     * @param offsetY
     * @param tileX -- if you would like to draw an image that is not a tile set this to 0
     * @param tileY -- if you would like to draw an image that is not a tile set this to 0
     */
    public void drawImage(Image image, int offsetX, int offsetY , int tileX, int tileY)
    {	
 
        if (image.isAlpha() && !processing)
        {
            imageRequest.add(new ImageRequest(image, zDepth, offsetX, offsetY));
            return;
        }
        //Rendering optimizations
        //Code to not render anything completely off screen
        if (offsetX < -image.getWidth() || offsetY < -image.getHeight() || offsetX >= pixelWidth || offsetY >= pixelHeight) return;

        int newX = 0, newY = 0, newWidth = image.getWidth(), newHeight = image.getHeight(), tileHeight = 1, tileWidth = 1;

        //Code to not render pixels that are clipping off the edge
        if (offsetX < 0) newX -= offsetX;
        if (offsetY < 0) newY -= offsetY;
        if (newWidth + offsetX >= pixelWidth) newWidth -= newWidth + offsetX - pixelWidth;
        if (newHeight + offsetY >= pixelHeight) newHeight -= newHeight + offsetY - pixelHeight;
        //End rendering optimizations


        if(image.getClass() == ImageTile.class){
        	ImageTile im = (ImageTile) image;
            newWidth = im.getTileWidth();
            newHeight = im.getTileHeight();
            tileHeight = im.getTileHeight();
            tileWidth = im.getTileWidth(); 
        }
        
        for (int y = newY; y < newHeight; y++)
        {
            for (int x = newX; x < newWidth; x++)
            {
                setPixel(x + offsetX, y + offsetY, image.getPixel()[(x + tileX *  tileWidth) + (y + tileY * tileHeight) * image.getWidth()]);
                setLightBlock(x + offsetX, y + offsetY, image.getLightBlock());
            }
        }
        
    }

   

    public void drawRect(int offsetX, int offsetY, int width, int height, int color)
    {
        for (int y = 0; y <= height; y++)
        {
            setPixel(offsetX, y + offsetY, color);
            setPixel(offsetX + width, y + offsetY, color);
        }
        for (int x = 0; x <= width; x++)
        {
            setPixel(x + offsetX, offsetY, color);
            setPixel(x + offsetX, offsetY + height, color);
        }
    }

    public void drawFilledRect(int offsetX, int offsetY, int width, int height, int color)
    {

        //Rendering optimizations
        //Code to not render anything completely off screen
        if (offsetX < -width || offsetY < -height || offsetX >= pixelWidth || offsetY >= pixelHeight) return;

        int newX = 0;
        int newY = 0;
        int newWidth = width;
        int newHeight = height;

        //Code to not render pixels that are clipping off the edge
        if (offsetX < 0) newX -= offsetX;
        if (offsetY < 0) newY -= offsetY;
        if (newWidth + offsetX >= pixelWidth) newWidth -= newWidth + offsetX - pixelWidth;
        if (newHeight + offsetY >= pixelHeight) newHeight -= newHeight + offsetY - pixelHeight;
        //End rendering optimizations


        for (int y = newY; y <= newHeight; y++)
        {
            for (int x = newX; x <= newWidth; x++)
            {
                setPixel(x + offsetX, y + offsetY, color);
            }
        }
    }
    
    public void drawLight(Light light, int offsetX, int offsetY)
    {
        lightRequest.add(new LightRequest(light, offsetX, offsetY));
    }
    
    private void drawLightRequest(Light light, int offsetX, int offsetY)
    {
        for (int i = 0; i <= light.getDiameter(); i++)
        {
            drawLightLine(light, light.getRadius(), light.getRadius(), i, 0, offsetX, offsetY);
            drawLightLine(light, light.getRadius(), light.getRadius(), i, light.getDiameter(), offsetX, offsetY);
            drawLightLine(light, light.getRadius(), light.getRadius(), 0, i, offsetX, offsetY);
            drawLightLine(light, light.getRadius(), light.getRadius(), light.getDiameter(), i, offsetX, offsetY);
        }
    }
    
    public void drawLightLine(Light light, int x0, int y0, int x1, int y1, int offsetX, int offsetY)
    {
        //no i didnt actually make this function
        //if this breaks david has to fix it
        int dx = Math.abs(x1 - x0);
        int dy = Math.abs(y1 - y0);
        int sx = x0 < x1 ? 1 : -1;
        int sy = y0 < y1 ? 1 : -1;
        int err = dx - dy;
        int err2;
        while (true)
        {
            int screenX = x0 - light.getRadius() + offsetX;
            int screenY = y0 - light.getRadius() + offsetY;
  
            if (screenX < 0 || screenX >= pixelWidth || screenY < 0 || screenY >= pixelHeight)
                return;
            
            int lightColor = light.getLightValue(x0, y0);
            if (lightColor == 0)
                return;
            
            if (lightBlock[screenX + screenY * pixelWidth] == Light.FULL)
                return;
            
            setLightMap(screenX, screenY, lightColor);
            if (x0 == x1 && y0 == y1)
                break;
            err2 = 2 * err;
            if (err2 > -1 * dy)
            {
                err -= dy;
                x0 += sx;
            }
            if (err2 < dx)
            {
                err += dx;
                y0 += sy;
            }
        }
    }
}
