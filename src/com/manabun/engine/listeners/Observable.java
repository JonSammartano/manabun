package com.manabun.engine.listeners;

import com.manabun.engine.GameContainer;
import com.manabun.engine.Object;

public interface Observable {
	public void addObserver(Object o);
	public void removeObserver(Object o);
	public void notifyObservers(GameContainer gc, float dt);
}
